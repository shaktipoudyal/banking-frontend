import React, { Component } from "react";
import { Input, Form, Modal, Table, Icon, Checkbox } from 'semantic-ui-react'
import {Button} from "react-bootstrap";
import Axios from "axios";
import './../../src/newaccountform.css';
import * as API_CONSTANT from '../ApiConstants';

class NewAccountForm extends Component{
    constructor(props){ //bind methods to component
        super(props);
        this.state={
            columns:[
                {"header":"First Name", "name": "firstName"},
                {"header":"Last Name", "name": "lastName"},
                {"header":"SSN", "name": "ssn"},
                {"header":"Location", "name": "location"},
                {"header":"Active Accounts", "name": "activeAccounts"}
            ],
           ssn:false,
           activeAccounts: "N/A",
           selectedOpitions: new Array(10),
           data: [],
           selectedAccounts: [],
           checkingAmt: null,
           savingsAmt: null,
           businessAmt: null,
           accountType: null,
           accountTypeArray: [],
           amt: null,
           customerId: null,
           firstName: null,
           lastName: null,
           ssn: null,
           errorMsg: '',
           jwtToken: localStorage.getItem('jwtInLocalStorage') || ''
        };
        this.handleOnChange= this.handleOnChange.bind(this); 
        this.handleAddAccount= this.handleAddAccount.bind(this);
        this.handleSubmitAdd= this.handleSubmitAdd.bind(this);
        this.handleOnChangeAmounts= this.handleOnChangeAmounts.bind(this);
        this.handleGetCustomerBySSN= this.handleGetCustomerBySSN.bind(this);
    }

    handleOnChangeAmounts= (row)=>event =>{
        event.preventDefault();
        let obj={};
        let holdValue=event.target.value;
        obj[event.target.id]=event.target.value;

        {
            console.log("hold value $$$", holdValue);
            console.log("row: ", row)
            console.log(event)
            console.log(event.target.value)
        }

        if(row==0){
            this.setState({
                checkingAmt: holdValue,
            })
        }else if(row==1){
            this.setState({
                savingsAmt: holdValue,
            })
        }else{
            this.setState({
                businessAmt: holdValue,
            })
        }
    }

    handleOnChange= (event) =>{
        event.preventDefault();
        let obj={};
        obj[event.target.id]=event.target.value;
        console.log(event.value)

        this.setState({
            ...this.state.ssn,
            ...obj
        })
    }

    handleSelectCheckbox= id=>(event)=> {
        event.preventDefault();
        console.log(event);
        console.log(id);

        let selected = [];
        if(id!=null){
            if(id==1){
                this.setState({
                    selectedOpitions: [...this.state.selectedOpitions, "Checking"]
                })
            }else if(id==2){
                this.setState({
                    selectedOpitions: [...this.state.selectedOpitions, "Savings"]
                })
            }else{
                this.setState({
                    selectedOpitions: [...this.state.selectedOpitions, "Business Checking"]
                })
            }
        }
 
        console.log("selectedOpitions", this.state.selectedOpitions);
    }

    handleAddAccount(event){
        event.preventDefault();
        console.log(this.state.selectedOpitions);
        var excludeUnchecked=[];
        var counts = {};
        for (var i = 0; i < this.state.selectedOpitions.length; i++) {
            var num = this.state.selectedOpitions[i];
            counts[num] = counts[num] ? counts[num] + 1 : 1;
        }

        Object.entries(counts).map(([key, value]) =>
            {
                if(value%2!=0){
                    excludeUnchecked.push(key)
                }
            }
        )
        console.log("excludeUnchecked", excludeUnchecked);

        this.setState({
            selectedAccounts: excludeUnchecked
        })
        console.log("excludeUnchecked", excludeUnchecked)
        console.log("this.state.selectedAccounts", this.state.selectedAccounts);

    }

    handleGetCustomerBySSN(event){
        event.preventDefault();
        Axios({
            url: API_CONSTANT.BASE_URL + '/api/getCustomerBySSN/' + this.state.ssn,
            method: "get",
            headers:{
                "Content-Type":"application/json",
                "Authorization": "Bearer " + this.state.jwtToken
            }
        }).then(res=>{
            this.setState({
                data: res.data,
                accountType: res.data.accounts,
                accountTypeArray: res.data.accounts,
                customerId: res.data.id,
                firstName: res.data.firstName,
                lastName: res.data.lastName,
                ssn: res.data.ssn
            })
            console.log(this.state.data)
            console.log(res.data.accounts[0].accountType)
        }).catch(err=>{
            this.setState({
                errorMsg: 'Not found'
            })
            console.log(err);
        });
    }

    handleSubmitAdd(event){
        event.preventDefault();
        console.log(this.state.checkingAmt);
        console.log(this.state.savingsAmt);
        console.log(this.state.businessAmt);
        console.log(this.state.selectedAccounts);

        let chekingAccount={};
        let savingsAccount={};
        let businessCheckingAccount={};
        let id= this.state.customerId;
        let firstName= this.state.firstName;
        let lastName= this.state.lastName;
        let ssn= this.state.ssn;

        if(this.state.selectedAccounts.indexOf('Checking')> -1){
            chekingAccount["accountType"]= "Checking";
            chekingAccount["amount"]= this.state.checkingAmt;
        }
        if(this.state.selectedAccounts.indexOf('Savings')> -1){
            savingsAccount["accountType"]= "Saving";
            savingsAccount["amount"]= this.state.savingsAmt;
        }
        if(this.state.selectedAccounts.indexOf('Business Checking')> -1){
            businessCheckingAccount["accountType"]= "Business_Checking";
            businessCheckingAccount["amount"]= this.state.businessAmt;
        }

        console.log( Object.keys(savingsAccount).length === 0)

        let accounts=[];
        if(Object.keys(chekingAccount).length > 0){
            accounts.push(chekingAccount);
        }
        if(Object.keys(savingsAccount).length > 0){
            accounts.push(savingsAccount);
        } 
        if(Object.keys(businessCheckingAccount).length > 0){
            accounts.push(businessCheckingAccount);
        }

        console.log(accounts);
        console.log(accounts[0]);
        
        let userData= {
            id,
            firstName,
            lastName,
            ssn,
            accounts
        }

        console.log(userData)
        Axios({
            url: API_CONSTANT.BASE_URL + '/api/openAccount',
            method: "post",
            data: userData,
            headers:{
                "Content-Type":"application/json",
                "Authorization": "Bearer " + this.state.jwtToken
            }
        }).then(res=>{
            this.props.closeForm();
            this.setState({
               
            })
        }).catch(err=>{
            console.log(err);
        });
    }

    render(){
        {
            console.log(this.state.data);
        }
        {
            console.log(this.state.accountTypeArray);
        }
       
        let customerData= this.state.data;
        let customerDataAccounts= this.state.accountTypeArray;
        
        const renderCustomerInfo=()=>{
            if(customerData.length==0){
                return (
                    <div></div>
                )
            }else{
                return (
                    <div className="customer_info_table" margin-top="5px">
                        <Table basic>
                            <Table.Header>
                                <Table.Row>
                                    {/* <Table.HeaderCell>&nbsp;</Table.HeaderCell> */}
                                    {this.state.columns.map(col=>
                                        <Table.HeaderCell key={col.name}>{col.header}</Table.HeaderCell>
                                    )}
                                </Table.Row>
                            </Table.Header>

                            {/* <Table.Body>
                                {Object.entries(customerData).map((row, index)=>
                                <Table.Row key={row.id}>
                                    <Table.Cell>
                                        <Icon name= 'edit' style={{cursor:'pointer'}} color='blue' size='med'></Icon>
                                    </Table.Cell>
                                    <Table.Cell>{customerData.firstName}</Table.Cell>
                                    <Table.Cell>{customerData.lastName}</Table.Cell>
                                    <Table.Cell>{customerData.ssn}</Table.Cell>
                                    <Table.Cell>{customerData.address.city}, {customerData.address.stateCode}</Table.Cell>
                                    <Table.Cell>{this.state.activeAccounts}</Table.Cell>
                                </Table.Row>
                                )}
                            </Table.Body> */}
                            <Table.Body>
                                    <Table.Cell>{customerData.firstName}</Table.Cell>
                                    <Table.Cell>{customerData.lastName}</Table.Cell>
                                    <Table.Cell>{customerData.ssn}</Table.Cell>
                                    <Table.Cell>{customerData.address.city}, {customerData.address.stateCode}</Table.Cell>
                                    <Table.Cell>{customerDataAccounts.map((acc => (<li>{acc.accountType}</li>)))}</Table.Cell>
                                    <Table.Cell>
                                        <Icon name= 'edit' style={{cursor:'pointer'}} color='blue' size='med'></Icon>
                                    </Table.Cell>
                            </Table.Body>
                        </Table>
                    </div>
                );
            }
        }

        const renderOptions=()=>{          
            let containsChecking= customerDataAccounts.some(account=> account['accountType'] === 'Checking' )
            let containsSaving= customerDataAccounts.some(account=> account['accountType'] === 'Saving' )
            let containsBusinessChecking= customerDataAccounts.some(account=> account['accountType'] === 'Business_Checking' )

            if(customerData.length==0){
                return (
                    <div style={{fontSize:12, color:"red", fontStyle:"italic"}}>
                        {this.state.errorMsg}
                    </div>
                );
            }  else if(containsChecking==true && containsSaving==true && containsBusinessChecking==true) {
                return (
                    <div>
                        <strong>Customer is already enrolled to all accounts...</strong>
                    </div>
                );
            } else if(containsChecking==true && containsSaving!=true && containsBusinessChecking!=true){
                return (
                    <div className= "checkboxes">
                        <strong>Select Account(s): </strong>
                            <span className= "checkbox-2"><Checkbox label='Saving' onClick={this.handleSelectCheckbox(2)}/></span>
                            <span className= "checkbox-3"><Checkbox label='Business Checking' onClick={this.handleSelectCheckbox(3)}/></span>
                            <span className= "icon"><Icon link name='plus circle' size='big' color='green' onClick={this.handleAddAccount} label="Add Selected Account(s)" title="Add Selected Account(s)"/></span>
                    </div>
                );
            } else if(containsChecking!=true && containsSaving==true && containsBusinessChecking!=true){
                return (
                    <div className= "checkboxes">
                        <strong>Select Account(s): </strong>
                        <span className= "checkbox-1"><Checkbox label='Checking' onClick={this.handleSelectCheckbox(1)}/></span>
                        <span className= "checkbox-3"><Checkbox label='Business Checking' onClick={this.handleSelectCheckbox(3)}/></span>
                        <span className= "icon"><Icon link name='plus circle' size='big' color='green' onClick={this.handleAddAccount} label="Add Selected Account(s)" title="Add Selected Account(s)"/></span>
                    </div>
                );
            } else if(containsChecking!=true && containsSaving!=true && containsBusinessChecking==true){
                return (
                    <div className= "checkboxes">
                        <strong>Select Account(s): </strong>
                        <span className= "checkbox-1"><Checkbox label='Checking' onClick={this.handleSelectCheckbox(1)}/></span>
                        <span className= "checkbox-2"><Checkbox label='Saving' onClick={this.handleSelectCheckbox(2)}/></span>
                        <span className= "icon"><Icon link name='plus circle' size='big' color='green' onClick={this.handleAddAccount} label="Add Selected Account(s)" title="Add Selected Account(s)"/></span>
                    </div>
                );
            } else if(containsChecking==true && containsSaving==true && containsBusinessChecking!=true) {
                return (
                    <div className= "checkboxes">
                        <strong>Select Account(s): </strong>
                        <span className= "checkbox-3"><Checkbox label='Business Checking' onClick={this.handleSelectCheckbox(3)}/></span>
                        <span className= "icon"><Icon link name='plus circle' size='big' color='green' onClick={this.handleAddAccount} label="Add Selected Account(s)" title="Add Selected Account(s)"/></span>
                    </div>
                );
            } else if(containsChecking==true && containsSaving!=true && containsBusinessChecking==true) {
                return (
                    <div className= "checkboxes">
                        <strong>Select Account(s): </strong>
                        <span className= "checkbox-2"><Checkbox label='Saving' onClick={this.handleSelectCheckbox(2)}/></span>
                        <span className= "icon"><Icon link name='plus circle' size='big' color='green' onClick={this.handleAddAccount} label="Add Selected Account(s)" title="Add Selected Account(s)"/></span>
                    </div>
                );
            } else if(containsChecking!=true && containsSaving==true && containsBusinessChecking==true) {
                return (
                    <div className= "checkboxes">
                        <strong>Select Account(s): </strong>
                        <span className= "checkbox-1"><Checkbox label='Checking' onClick={this.handleSelectCheckbox(1)}/></span>
                        <span className= "icon"><Icon link name='plus circle' size='big' color='green' onClick={this.handleAddAccount} label="Add Selected Account(s)" title="Add Selected Account(s)"/></span>
                    </div>
                );
            }else{
                if(customerData.length!=0 && customerData.accounts[0]==null){
                    return (
                        <div className= "checkboxes">
                            <strong>Select Account(s): </strong>
                                <span className= "checkbox-1"><Checkbox label='Checking' onClick={this.handleSelectCheckbox(1)}/></span>
                                <span className= "checkbox-2"><Checkbox label='Saving' onClick={this.handleSelectCheckbox(2)}/></span>
                                <span className= "checkbox-3"><Checkbox label='Business Checking' onClick={this.handleSelectCheckbox(3)}/></span>
                                <span className= "icon"><Icon link name='plus circle' size='big' color='green' onClick={this.handleAddAccount} label="Add Selected Account(s)" title="Add Selected Account(s)"/></span>
                        </div>
                    );
                }
            }
        }

        const renderSecondaryForm=()=>{
            let containsChecking= this.state.selectedAccounts.indexOf("Checking") > -1
            let containsSaving= this.state.selectedAccounts.indexOf("Savings") > -1
            let containsBusinessChecking= this.state.selectedAccounts.indexOf("Business Checking") > -1

            // get rid of null and undefined values
            this.state.selectedAccounts.filter(item => item !== undefined && item !== null);

            console.log(this.state.selectedAccounts);
            console.log(this.state.selectedAccounts.length);
            console.log(this.state.selectedAccounts.some(account=> account['accountType'] === 'Business Checking' ))

            if(this.state.selectedAccounts.length == 3){
                return (
                    <div className="accounts_table">
                        <Table basic>
                        <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                                    {this.state.selectedAccounts.map(col=>
                                        // <Table.HeaderCell key={col.name}>{col.header}</Table.HeaderCell>
                                        <Table.HeaderCell>{col}</Table.HeaderCell>
                                    )}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                <Table.Cell><strong>Deposit Amount (USD): </strong></Table.Cell>
                                {this.state.selectedAccounts.map((row, index)=>
                                // <Table.Column key={row.id}>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(index)}/></Table.Cell>
                                // </Table.Column>
                                )}
                            </Table.Body>
                        </Table>

                        <Button variant="success" onClick={this.handleSubmitAdd}>SUBMIT</Button>
                    </div>
                )
            } else if(this.state.selectedAccounts.length == 2 && containsChecking == true && containsSaving == true){
                return (
                    <div className="accounts_table">
                        <Table basic>
                        <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                                    {this.state.selectedAccounts.map(col=>
                                        <Table.HeaderCell>{col}</Table.HeaderCell>
                                    )}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                <Table.Cell><strong>Deposit Amount (USD): </strong></Table.Cell>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(0)}/></Table.Cell>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(1)}/></Table.Cell>
                            </Table.Body>
                        </Table>
                        <Button variant="success" onClick={this.handleSubmitAdd}>SUBMIT</Button>
                    </div>
                )
            } else if(this.state.selectedAccounts.length == 2 && containsChecking == true && containsBusinessChecking == true){
                return (
                    <div className="accounts_table">
                        <Table basic>
                        <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                                    {this.state.selectedAccounts.map(col=>
                                        <Table.HeaderCell>{col}</Table.HeaderCell>
                                    )}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                <Table.Cell><strong>Deposit Amount (USD): </strong></Table.Cell>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(0)}/></Table.Cell>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(2)}/></Table.Cell>
                            </Table.Body>
                        </Table>
                        <Button variant="success" onClick={this.handleSubmitAdd}>SUBMIT</Button>
                    </div>
                )
            } else if(this.state.selectedAccounts.length == 2 && containsBusinessChecking == true && containsSaving == true){
                return (
                    <div className="accounts_table">
                        <Table basic>
                        <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                                    {this.state.selectedAccounts.map(col=>
                                        <Table.HeaderCell>{col}</Table.HeaderCell>
                                    )}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                <Table.Cell><strong>Deposit Amount (USD): </strong></Table.Cell>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(1)}/></Table.Cell>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(2)}/></Table.Cell>
                            </Table.Body>
                        </Table>
                        <Button variant="success" onClick={this.handleSubmitAdd}>SUBMIT</Button>
                    </div>
                )
            } else if(this.state.selectedAccounts.length == 1 && containsBusinessChecking == true){
                return (
                    <div className="accounts_table">
                        <Table basic>
                        <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                                    {this.state.selectedAccounts.map(col=>
                                        <Table.HeaderCell>{col}</Table.HeaderCell>
                                    )}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                <Table.Cell><strong>Deposit Amount (USD): </strong></Table.Cell>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(2)}/></Table.Cell>
                            </Table.Body>
                        </Table>
                        <Button variant="success" onClick={this.handleSubmitAdd}>SUBMIT</Button>
                    </div>
                )
            } else if(this.state.selectedAccounts.length == 1 && containsSaving == true){
                return (
                    <div className="accounts_table">
                        <Table basic>
                        <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                                    {this.state.selectedAccounts.map(col=>
                                        <Table.HeaderCell>{col}</Table.HeaderCell>
                                    )}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                <Table.Cell><strong>Deposit Amount (USD): </strong></Table.Cell>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(1)}/></Table.Cell>
                            </Table.Body>
                        </Table>
                        <Button variant="success" onClick={this.handleSubmitAdd}>SUBMIT</Button>
                    </div>
                )
            } else if(this.state.selectedAccounts.length == 1 && containsChecking == true){
                return (
                    <div className="accounts_table">
                        <Table basic>
                        <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                                    {this.state.selectedAccounts.map(col=>
                                        <Table.HeaderCell>{col}</Table.HeaderCell>
                                    )}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                <Table.Cell><strong>Deposit Amount (USD): </strong></Table.Cell>
                                    <Table.Cell><input type='number' onChange={this.handleOnChangeAmounts(0)}/></Table.Cell>
                            </Table.Body>
                        </Table>
                        <Button variant="success" onClick={this.handleSubmitAdd}>SUBMIT</Button>
                    </div>
                )
            }
        }
        return(
            <div>
                <Form>
                    <Form.Input
                            width={5}
                            fluid
                            required
                            type="number"
                            label="Look up customer by SSN: "
                            id="ssn"
                            name="ssn"
                            onChange={this.handleOnChange}
                            value={this.state.ssn}
                            placeholder="Enter full customer SSN (9 digits)"
                        />
                </Form>
                <Modal.Actions>
                        <Button variant="success" className="btn-1" onClick={this.handleGetCustomerBySSN} disabled={!this.state.ssn || this.state.ssn.length!=9}>
                            Search Customer
                        </Button>
                        <Button variant="danger" className="btn-2" onClick={this.props.closeForm}>
                            Close
                        </Button>
                </Modal.Actions>

                {
                    renderCustomerInfo()
                }

                {
                    renderOptions()
                }

                {
                    renderSecondaryForm()
                }
                
            </div>
        );
    }
}

export default NewAccountForm;