import React, { Component } from 'react';
import {Card, Navbar, Container, Button, Nav, NavDropdown} from "react-bootstrap";
import {Modal, MenuItem, Form} from 'semantic-ui-react/dist/commonjs';
import {landingpage} from './../landingpage.css';
import NewCustomerForm from './NewCustomerForm';
import Axios from 'axios';
import ExistingCustomers from './ExistingCustomers';
import NewAccountForm from './NewAccountForm';
import LandingPageContent from './LandingPageContent';

import * as API_CONSTANT from '../ApiConstants';

class LandingPageMenu extends Component{
    constructor(props){ //bind methods to component
        super(props);
        this.state={
            newCustomerForm: false,
            newAccountForm: false,
            showCustomers: false,
            data:[],
            loggedInUser: localStorage.getItem('userNameInLocalStorage') || '',
            jwtToken: localStorage.getItem('jwtInLocalStorage') || ''
        };

        this.showNewCustomerForm= this.showNewCustomerForm.bind(this);
        this.showNewAccountForm= this.showNewAccountForm.bind(this);
        this.closeForm= this.closeForm.bind(this);
        this.handleExistingCustomers= this.handleExistingCustomers.bind(this);
        this.handleToggleLogIn= this.handleToggleLogIn.bind(this);
    }

    handleToggleLogIn(event){
       
    }

    // toggleLogOutState(event){
    //     localStorage.setItem('jwtInLocalStorage', null);
    //     this.setState({
    //         toggleLoginState: false
    //     })
    // }

    showNewCustomerForm(event){
        this.setState({
            newCustomerForm: true
        })
    }

    showNewAccountForm(event){
        this.setState({
            newAccountForm: true
        })
    }

    closeForm(event){
        this.setState({
            newCustomerForm: false,
            showCustomers: false,
            newAccountForm: false
        })
    }

    handleExistingCustomers(event){
        Axios({
            url: API_CONSTANT.BASE_URL + '/api/getCustomers',
            method:"get",
            headers:{
                "Content-Type":"application/json",
                "Authorization": "Bearer " + this.state.jwtToken
            }
        }).then(res=>{
            this.setState({
                data: res.data,
                showCustomers: true
            })
            console.log(this.state.data)
        }).catch(err=>{
            console.log(err);
        });
    }

    render(){
        const title="Cerotid Banking";
        return (
            <React.Fragment className="container">
                <div className="card_body">
                <Card.Body>
                    <div className="pageTitle"><h2>{title}<h6>Employee Portal</h6></h2></div>
                    <div className="loggedInUser">Welcome, <strong>{this.state.loggedInUser}!</strong></div>
                </Card.Body>
                </div>
                    <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
                    <Navbar.Brand href="#home">Home</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                        <NavDropdown title="Customers" id="collasible-nav-dropdown">
                            <NavDropdown.Item ref="#action/1.1"onClick={this.showNewCustomerForm}>Create New Customer</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/1.2" onClick={this.handleExistingCustomers}>Existing Customers</NavDropdown.Item>
                            {/* <MenuItem onClick={this.showNewCustomerForm}>an item</MenuItem> */}
                        </NavDropdown>
                        <NavDropdown title="Accounts" id="collasible-nav-dropdown">
                            <NavDropdown.Item ref="#action/2.1"onClick={this.showNewAccountForm}>Create An Account</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/2.2" onClick={this.handleExistingCustomers}>Existing Accounts</NavDropdown.Item>
                            {/* <MenuItem onClick={this.showNewCustomerForm}>an item</MenuItem> */}
                        </NavDropdown>
                        <NavDropdown title="Transactions" id="collasible-nav-dropdown">
                            <NavDropdown.Item href="#action/2.1">Transfer Money</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/2.2">Transfer History</NavDropdown.Item>
                        </NavDropdown>
                        {/* <Nav.Link href="#features">Transactions</Nav.Link> */}
                        </Nav>
                        <Nav>
                        <Nav.Link href="#deets">Contact Us</Nav.Link>
                        </Nav>
                        <Nav>
                            <Nav.Link onClick={this.props.toggleLogOutState} href="/">Sign Out</Nav.Link>
                        </Nav>

                        {/* <Nav>
                        <Nav.Link href="#deets">More deets</Nav.Link>
                        <Nav.Link eventKey={2} href="#memes">
                            Dank memes
                        </Nav.Link>
                        </Nav> */}
                    </Navbar.Collapse>
                    </Navbar>

                    <LandingPageContent/>
                    
                    <Modal open={this.state.newCustomerForm} close={!this.state.newCustomerForm}>
                        <Modal.Header content= {"Add New Customer"}/>
                        <Modal.Content>
                          <NewCustomerForm closeForm={this.closeForm}/>
                        </Modal.Content>
                    </Modal>

                    <Modal open={this.state.showCustomers} close={!this.state.showCustomers}>
                        <Modal.Header content= {"Existing Customers"}/>
                        <Modal.Content>
                           <ExistingCustomers rows={this.state.data} closeForm={this.closeForm}/>
                        </Modal.Content>
                    </Modal>

                    <Modal open={this.state.newAccountForm} close={!this.state.newAccountForm}>
                        <Modal.Header content= {"Add New Account"}/>
                        <Modal.Content>
                           <NewAccountForm closeForm={this.closeForm}/>
                        </Modal.Content>
                    </Modal>
            </React.Fragment>
        );
    }
}
export default LandingPageMenu;