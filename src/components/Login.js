import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Form, Button} from 'semantic-ui-react'
import './../loginpage.css';
import {Responsive, Header, Grid, Modal} from 'semantic-ui-react/dist/commonjs';
import Axios from "axios";
import { BrowserRouter ,Switch , Route, Router, Redirect } from 'react-router-dom';
import Register from './Register';
import { withRouter } from 'react-router-dom';
import NewCustomerForm from './NewCustomerForm';
import * as API_CONSTANT from '../ApiConstants';

class Login extends Component{
    constructor(props){ //binds methods to component
        super(props);
        this.state={
            userName:'',
            password:'',
            loggedInState: false,
            statusCode: null,
            jwtToken: null,
            loginError: null,
            data: [],
            registerForm:false
        };
        this.handleLogin= this.handleLogin.bind(this);
        this.handleRegister= this.handleRegister.bind(this);
    }

    handleOnChange= (event) =>{
        let obj={};
        obj[event.target.id]=event.target.value;

        this.setState({
            ...this.state.userName,
            ...this.state.password,
            ...obj
        })
    }

    handleLogin(event){
        let userData={
            username: this.state.userName,
            password: this.state.password,
        };
        console.log(userData)
        Axios({
            url: API_CONSTANT.BASE_URL + '/userlogin/authenticate',
            method: "post",
            data: userData,
            headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Origin":"*"
            }
        }).then(res=>{
           
            console.log(res)
            console.log(res.status)
            console.log(res.data.jwt)
            const jwtTokenz = res.data.jwt;
            localStorage.setItem('jwtInLocalStorage', res.data.jwt);
            if(res.status==200){
                this.props.toggleLoginState();
            }
            this.setState({
                data: res.data,
                jwtToken: jwtTokenz
            })
        })
        .catch(error=> {
            this.setState({
                loginError: "**INVALID USERNAME OR PASSWORD"
            })
        });
        console.log(this.state.jwtToken)
    }

    handleRegister(event){
        if(this.state.registerForm==true){
            this.setState({
                registerForm:false
            })
        }else{
            this.setState({
                registerForm:true
            })
        }
    }

    render(){
        localStorage.setItem('userNameInLocalStorage', this.state.userName);

        return (
            <div className="App" style={{marginTop:50}}>
            
                <Responsive>
                    <Header as='h1'>LOGIN PAGE</Header>

                <Form className="loginform">
                    <Form.Input className="loginform_username"
                        width={5}
                        fluid
                        required
                        type="text"
                        label="Username:"
                        id="userName"
                        name="userName"
                        placeholder="Enter your username"
                        onChange={this.handleOnChange}
                    />
                </Form>
                <Form className="loginform">
                    <Form.Input className="loginform_password"
                        width={5}
                        fluid
                        required
                        type="password"
                        label="Password:"
                        id="password"
                        name="password"
                        placeholder="Enter your password"
                        onChange={this.handleOnChange}
                    />
                </Form>
                    <div style={{fontSize:12, color:"red", fontStyle:"italic"}}>{this.state.loginError}</div>
                    {/* <div>
                        <Link className="btn btn-primary" to="/register">Register Here</Link>
                    </div> */}
               <div><Link><i>Forgot Username / Password?</i></Link></div>
                <Button.Group style={{marginTop:20}}>
                    <Button as={Link} to='/dashboard' style={{marginRight:10}} disabled={!this.state.userName || !this.state.password} onClick={this.handleLogin}>LOG IN</Button>
                    <Button as={Link} to='/register' onClick={this.handleRegister}>REGISTER</Button>
                </Button.Group>
                </Responsive>

                <Modal open={this.state.registerForm} close={!this.state.registerForm}>
                        <Modal.Content>
                           <Register handleRegister={this.handleRegister}/>
                        </Modal.Content>
                </Modal>
            </div>
        );
    }
}
export default withRouter(Login);