import React, { Component } from 'react';
import {Modal, MenuItem, Form} from 'semantic-ui-react/dist/commonjs';
import {Button} from "react-bootstrap";
import Axios from "axios";
import * as API_CONSTANT from '../ApiConstants';

class UpdateCustomer extends Component{
    constructor(props){ //binds methods to component
        super(props);
        this.state={
           data: this.props.rows,
           jwtToken: localStorage.getItem('jwtInLocalStorage') || ''
        };
        this.handleDelete= this.handleDelete.bind(this);
    }

    handleDelete(event){
        this.props.handleDeleteCustomer(0);
        Axios({
            url: API_CONSTANT.BASE_URL + '/api/deleteCustomer/' + + this.state.data.id, 
            method: "delete",
            headers:{
                "Content-Type":"application/json",
                "Authorization": "Bearer " + this.state.jwtToken
            }
        }).then(res=>{
            // this.props.handleDeleteCustomer(0);
            this.setState({
            })
        })
        
    }

    render(){
        console.log(this.state.data)
        let firstName= this.state.data.firstName;
        let lastName= this.state.data.lastName;
        let ssn= this.state.data.ssn;
        return(
            <div>
                <Modal.Content>
                    <div>Customer Name: <strong>{firstName} {lastName}</strong></div>
                    <div>SSN: <strong>{ssn}</strong></div>
                </Modal.Content>
                <Modal.Actions>
                    <Button variant="success" className="btn-2" color='orange' onClick={this.handleDelete}>
                        Confirm
                    </Button>
                    <Button variant="danger" className="btn-2" onClick={this.props.handleDeleteCustomer(0)}>
                        Close
                    </Button>
                </Modal.Actions>
            </div>
        );
    }
}
export default UpdateCustomer;