import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'semantic-ui-css/semantic.min.css';
import LandingPageMenu from './components/LandingPageMenu';
import { Switch , Route, Redirect } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Register from './components/Register';
import Login from './components/Login';
import {Form, Button} from 'semantic-ui-react'
import {Link} from 'react-router-dom';

class App extends Component{
    constructor(){
        super();
        this.state={
            loggedInState: false,
            registerState: false,
            loginUsername: null,
            loginPassword: null,
            retainLogInState: false,
            data:null,
        }
        this.toggleLoginState= this.toggleLoginState.bind(this);
        this.toggleLogOutState= this.toggleLogOutState.bind(this);
        this.toggleRegisterState= this.toggleRegisterState.bind(this);        
    }

    toggleLoginState(event) {
        this.setState({
            loggedInState: true
        })
    }

    toggleLogOutState(event) {
        localStorage.setItem('jwtInLocalStorage', null);
        this.setState({
            loggedInState: false,
            retainLogInState: false
        })
    }

    toggleRegisterState(event) {
        this.setState({
            registerState: true
        })
    }

    render(){
        console.log(localStorage.getItem('jwtInLocalStorage'));
        console.log(typeof(localStorage.getItem('jwtInLocalStorage')));
        console.log(this.state.loggedInState);

        if(this.state.loggedInState==true || (localStorage.getItem('jwtInLocalStorage')!='null')){
            return (
                <Router>
                {/* <Switch> */}
                <Route path="/dashboard/" exact/>
                <div className="App"> <LandingPageMenu toggleLogOutState={this.toggleLogOutState}/></div>
                {/* </Switch> */}
                </Router>
            );
        }

        if(this.state.registerState==true){
            return (
                <Router>
                    <Route path="/register"exact/>
                    <div className="App"> <Register/></div>
               </Router>
            );
        }

        
        return (             
            <Router>
                <Route path="/" exact/>
                <div className="App">
                <Login toggleLoginState={this.toggleLoginState} toggleRegisterState={this.toggleRegisterState}/>
                
                {/* <Register/> */}
               
                {/* <Switch>
                    <Route path="/register" component={Register}/>
                    <Route exact path="/login" component={Login}/>
                </Switch> */}
                </div>
            </Router>
        );
    }
}
export default App;