import React, { Component } from 'react';
import {Card, Navbar, Container, Button, Nav, NavDropdown} from "react-bootstrap";
import {Modal, MenuItem, Form, Dropdown} from 'semantic-ui-react';
import Axios from "axios";
import {US_STATES_LIST} from "./../CONSTANTS/US_STATES";
import * as API_CONSTANT from '../ApiConstants';

class NewCustomerForm extends Component{
    constructor(props){ //bind methods to component
        super(props);
        this.state={
            firstName: null,
            lastName: null,
            ssn: null,
            streetName: null,
            city: null,
            stateCode: null,
            zipCode: null,
            jwtToken: localStorage.getItem('jwtInLocalStorage') || ''

        };
        this.handleSubmitAdd= this.handleSubmitAdd.bind(this);
        this.handleOnChange= this.handleOnChange.bind(this);
        this.handleSelectState= this.handleSelectState.bind(this);
       
    }

    us_states_options(){
        var us_states_options=[];
        for (let i = 1; i <= 10; i++) {
            us_states_options.push(<option key={i} value="{i}">{i}</option>)
        }
        return us_states_options;
    }

    handleOnChange= (event) =>{
        let obj={};
        obj[event.target.id]=event.target.value;

        console.log(this.state.stateCode)
        console.log(obj.stateCode)

        this.setState({
            ...this.state.firstName,
            ...this.state.lastName,
            ...this.state.ssn,
            ...this.state.city,
            ...this.state.zipCode,
            ...this.state.streetName,
            ...obj
        })
    }

    handleSelectState=(e,{value})=>{
    console.log("value" , value)
    this.setState({
        stateCode: value
    })
    }

    handleSubmitAdd(event){
        alert("handleSubmitAdd called inside new customer form")
        // event.preventDefault();
        let userData={
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            ssn: this.state.ssn,
            address:{
                streetName: this.state.streetName,
                city: this.state.city,
                stateCode: this.state.stateCode,
                zipCode: this.state.zipCode
            }
        };
        console.log(userData)
        console.log(this.state.jwtToken)

        Axios({
            url: API_CONSTANT.BASE_URL + '/api/registerCustomer',
            method: "post",
            data:userData,
            headers:{
                "Content-Type":"application/json",
                "Authorization": "Bearer " + this.state.jwtToken
            }
        }).then(res=>{
            this.props.closeForm()
            this.setState({
            })
        })
    }

    render(){
        console.log("jwt--", this.state.jwtToken);
        const title="Cerotid Banking";
        const customStyle={
            margin: "0px"
        }
        return (
                // <React.Fragment style={customStyle}>
                <div className="myModal">
                <Form>
                    <Form.Group widths="equal">
                    <Form.Input
                        fluid
                        required
                        type="text"
                        label="First Name"
                        id="firstName"
                        name="firstName"
                        onChange={this.handleOnChange}
                        value={this.state.firstName}
                    />
                    <Form.Input
                        fluid
                        required
                        type="text"
                        label="Last Name"
                        id="lastName"
                        name="lastName"
                        onChange={this.handleOnChange}
                        value={this.state.lastName}
                    />
                    </Form.Group>

                    <Form.Group>
                    <Form.Input
                        width={8}
                        fluid
                        required
                        type="number"
                        label="SSN"
                        id="ssn"
                        name="ssn"
                        onChange={this.handleOnChange}
                        value={this.state.ssn}
                    />
                    </Form.Group>

                    <Form.Group widths="equal">
                    <Form.Input
                        fluid
                        required
                        type="text"
                        label="Street Address"
                        id="streetName"
                        name="streetName"
                        onChange={this.handleOnChange}
                        value={this.state.streetName}
                    />
                        </Form.Group>
                        <Form.Group widths="equal">
                    <Form.Input
                        fluid
                        required
                        type="text"
                        label="City"
                        id="city"
                        name="city"
                        onChange={this.handleOnChange}
                        value={this.state.city}
                    />
                    <Form.Select
                        fluid
                        required
                        type="dropdown"
                        label="State"
                        id="stateCode"
                        name="stateCode"
                        options={US_STATES_LIST}
                        selection
                        onChange={this.handleSelectState}                        
                    />
                    <Form.Input
                        fluid
                        required
                        type="number"
                        max={9}
                        label="Zip Code"
                        id="zipCode"
                        name="zipCode"
                        onChange={this.handleOnChange}
                        value={this.state.zipCode}
                    />
                    </Form.Group>
                </Form>
                <Modal.Actions>
                        <Button 
                            variant="success" 
                            className="btn-1" 
                            onClick={this.handleSubmitAdd} 
                            disabled={!this.state.firstName || !this.state.lastName || !this.state.ssn 
                                    || this.state.ssn.length!=9 || !this.state.stateCode || !this.state.zipCode 
                                    || !this.state.city || !this.state.streetName}>
                            Submit
                        </Button>
                        <Button variant="danger" className="btn-2" onClick={this.props.closeForm}>
                            Close
                        </Button>
                </Modal.Actions>
                {/* </React.Fragment> */}
                </div>
            );
        }
    }
    export default NewCustomerForm;