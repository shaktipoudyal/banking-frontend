let US_STATES_LIST = [
    { key:'az', text:'Arizona', value:  'AZ' },
    { key: 'ar', text: 'Arkansas', value: 'AR' },
    { key: 'ca', text: 'California',  value: 'CA' },
    { key: 'co', text: 'Colorado', value: 'CO' },
    { key: 'ct', text: 'Connecticut', value:  'CT' },
    { key: 'de', text: 'Delaware',  value: 'DE' },
    { key: 'fl', text: 'Florida', value: 'FL' },
    { key: 'ga', text: 'Georgia', value: 'GA' },
    { key: 'hi', text: 'Hawaii',  value: 'HI' },
    { key: 'id', text: 'Idaho', value: 'ID' },
    { key: 'il', text: 'Illinois', value: 'IL' },
    { key: 'in', text: 'Indiana', value: 'IN' },
    { key: 'ia', text: 'Iowa', value: 'IA' },
    { key: 'ks', text: 'Kansas', value: 'KS' },
    { key: 'ky', text: 'Kentucky', value: 'KY' },
    { key: 'la', text: 'Louisiana', value: 'LA' },
    { key: 'me', text: 'Maine', value: 'ME' },
    { key: 'md', text: 'Maryland', value: 'MD' },
    { key: 'ma', text: 'Massachusetts', value: 'MA' },
    { key: 'mi', text: 'Michigan', value: 'MI' },
    { key: 'mn', text: 'Minnesota', value: 'MN' },
    { key: 'ms', text: 'Mississippi', value: 'MS' },
    { key: 'mo', text: 'Missouri', value:'MO' },
    { key: 'mt', text: 'Montana', value: 'MT' },
    { key: 'ne', text: 'Nebraska', value: 'NE' },  { key: 'nv', text: 'Nevada', value: 'NV' },  { key: 'ng', text: 'New Hampshire', value: 'NH' },  { key: 'nj', text: 'New Jersey', value: 'NJ' },  { key: 'nm', text: 'New Mexico', value: 'NM' },  { key: 'ny', text: 'New York', value: 'NY' },  { key: 'nc', text: 'North Carolina', value: 'NC' },  { key: 'nd', text: 'North Dakota', value: 'ND' },  { key: 'oh', text: 'Ohio', value: 'OH' },  { key: 'ok', text: 'Oklahoma', value: 'OK' },  { key: 'or', text: 'Oregon', value: 'OR' },  { key: 'pa', text: 'Pennsylvania', value: 'PA' },  { key: 'ri', text: 'Rhode Island', value: 'RI' },  { key: 'sc', text: 'South Carolina', value: 'SC' },  { key: 'sd', text: 'South Dakota', value: 'SD' },  { key: 'tn', text: 'Tennessee', value: 'TN' },  { key: 'tx', text: 'Texas', value: 'TX' },  { key: 'ut', text: 'Utah', value: 'UT' },  { key: 'vt', text: 'Vermont', value: 'VT' },  { key: 'va', text: 'Virginia', value: 'VA' },  { key: 'wa', text: 'Washington', value: 'WA'},  { key: 'wv', text: 'West Virginia', value: 'WV' },  { key: 'wi', text: 'Wisconsin', value: 'WI' },  { key: 'wy', text: 'Wyoming', value: 'WY' },  { key: 'gu', text: 'Guam', value: 'GU' },  { key: 'pr', text: 'Puerto Rico', value: 'PR' },  { key: 'vi', text: 'Virgin Islands', value: 'VI' }]
    
    // { value:  'AZ', label:  'ARIZONA' },
    // { value: 'AR', label:  'ARKANSAS' },
    // { value: 'CA', label:  'CALIFORNIA' }]

    export {US_STATES_LIST}