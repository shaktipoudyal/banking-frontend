import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Form, Button} from 'semantic-ui-react'
import {Responsive, Header} from 'semantic-ui-react/dist/commonjs';
import Axios from "axios";
import * as API_CONSTANT from '../ApiConstants';

import Login from './Login';
import { withRouter } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Switch , Route, Redirect } from 'react-router-dom';
// import './../loginpage.css';
import './../registerpage.css';

class Register extends Component{
    constructor(props){ //binds methods to component
        super(props);
        this.state={
            userName: null,
            password: null,
            confirmpassword: null,
            userNameError: null,
            passwordError: null,
            confirmpasswordError: null,
            disabledRegisterButtonDueToErrors: 'Error',
            userNameEligibility:null,
            // successfulRegistration: false

        };
        this.handleRegister= this.handleRegister.bind(this);
    }

    handleRegister(event){
        console.log(event.target.id)
        var clickedButtonId= event.target.id;
        console.log("register_btn?", event.target.id=='register_btn')
        var userName= this.state.userName;
        var password= this.state.password;
        var resetPassword= this.state.confirmpassword;

        var successfulRegistration;

        console.log( password);
        console.log(typeof password);
        console.log((new String(resetPassword).valueOf() == new String(password).valueOf()));
        console.log(resetPassword==password);

        if(resetPassword!=undefined && password!=undefined && resetPassword.length>0 && password.length>0){
            if(resetPassword.length!= password.length || (new String(resetPassword).valueOf() != new String(password).valueOf())){
                successfulRegistration= false;
                this.setState({
                    confirmpasswordError:'**Does not match password field!'
                })
            }else{
                successfulRegistration= true;
                this.setState({
                    confirmpasswordError:'',
                    // successfulRegistration: true
                })
            }
        }

        let userData;
        if(clickedButtonId=='register_btn'){
            userData={
                userName,
                password
            };
        }else{
            userData={
                userName
            };
        }
        if(successfulRegistration!=false){
            Axios({
                url: API_CONSTANT.BASE_URL + '/userlogin/register',
                method: "post",
                data: userData,
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Origin":"*"
                }
            }).then(res=>{
                let responseStatus= res.status;
                console.log("responseStatus", responseStatus)
                if(responseStatus==200) {  
                    if(clickedButtonId=='check_availablility_btn') {
                    successfulRegistration=false
                    console.log("Entering 200")
                    this.setState({
                        userNameEligibility: true,
                        userNameEligibilitySuccessMsg: '**Username Available To Register!',
                        userNameEligibilityMsg:'',
                    })
                    responseStatus= 0;
                    }else {
                        successfulRegistration= true;
                        this.setState({
                            // successfulRegistration: true,
                            userNameEligibility:false
                        })
                    }
                }else{
                    successfulRegistration= false;
                }
                this.setState({
                    data: res.data
                })

                if(successfulRegistration==true){
                    this.props.closeForm();
                }
            })
            .catch(error=> {
                console.log("catch successfulRegistration", successfulRegistration)
                successfulRegistration= false;
                console.log("successfulRegistration", successfulRegistration)
                console.log(clickedButtonId)
                // if(clickedButtonId=='check_availablility_btn'){
                    this.setState({
                        userNameEligibility: false,
                        userNameEligibilityMsg: '**Username is taken! Please try another username.',
                        userNameEligibilitySuccessMsg:''
                    })
            })
        } 
    }


    handleOnChange= (event) =>{
       
        let obj={};
        obj[event.target.id]=event.target.value;

        if(obj.userName!=undefined && obj.userName.length > 0){
            if(!obj.userName.match(/^[0-9a-zA-Z]+$/)){
                this.setState({
                    userNameError: "**Username can not contain special characters or spaces",
                    disabledRegisterButtonDueToErrors: 'Error'
                })
            }else if(obj.userName.length > 10){
                this.setState({
                    userNameError: "**Username length can not exceed 10 characters",
                    disabledRegisterButtonDueToErrors: 'Error',
                })
            }else{
               
                this.setState({
                    userNameError: '',
                    disabledRegisterButtonDueToErrors: '',
                    ...this.state.userName,
                    ...obj
                })
            }
        }

        if(obj.password!=undefined && obj.password.length > 0){
            if(obj.password.indexOf(' ') > 0 || obj.password.startsWith(' ')){
                this.setState({
                    passwordError: "**Password can not contain space(s)",
                    disabledRegisterButtonDueToErrors: 'Error',
                })
            }else if(obj.password.length > 10){
                this.setState({
                    passwordError: "**Password length can not exceed 10 characters",
                    disabledRegisterButtonDueToErrors: 'Error',
                })
            }else{
                
                this.setState({
                    passwordError: '',
                    disabledRegisterButtonDueToErrors: '',
                    ...this.state.password,
                    ...obj
                })
            }
        }

        if(obj.confirmpassword!=undefined && obj.confirmpassword.length != 0){
            this.setState({
                ...this.state.confirmpassword,
                ...obj
            })
        }
    }

    render(){
      
        return (
            <div>
                <Responsive>
                    <Header as='h1'>REGISTER</Header>
                <Form>
                    <Form.Input
                        width={5}
                        fluid
                        required
                        type="text"
                        label="Username:"
                        id="userName"
                        name="userName"
                        placeholder="Enter username"
                        onChange={this.handleOnChange}
                    />
                    <div className="password_notes" style={{fontSize:10, color:"blue", fontStyle:"italic"}}>Min length = 5, Max length = 10 characters<br/> Special characters and spaces not allowed</div>
                </Form>
                    <div className="userNameEligibilityMsg" style={{fontSize:13, color:"red", fontStyle:"italic"}}>{this.state.userNameEligibilityMsg}</div>
                    <div className="userNameEligibilitySuccessMsg" style={{fontSize:13, color:"green", fontStyle:"italic"}}>{this.state.userNameEligibilitySuccessMsg}</div>

                    <div className="userNameError" style={{fontSize:12, color:"red", fontStyle:"italic"}}>{this.state.userNameError}</div>
                    <div className="check_availablility"><Button id="check_availablility_btn" onClick={this.handleRegister} disabled={!this.state.userName || this.state.userName.length < 5 }>CHECK AVAILABILITY</Button></div>

                    <Form>
                    <Form.Input className="loginform_password"
                        width={5}
                        fluid
                        required
                        type="password"
                        label="Password:"
                        id="password"
                        name="password"
                        placeholder="Enter password"
                        onChange={this.handleOnChange}
                    />
                    <div className="password_notes" style={{fontSize:10, color:"blue", fontStyle:"italic"}}>Min length = 5, Max length = 10 characters</div>
                    </Form>
                    <div className="userNameError" style={{fontSize:12, color:"red", fontStyle:"italic"}}>{this.state.passwordError}</div>

                    <Form>
                     <Form.Input className="loginform_password"
                        width={5}
                        fluid
                        required
                        type="password"
                        label="Confirm Password:"
                        id="confirmpassword"
                        name="confirmpassword"
                        placeholder="Re-enter password"
                        onChange={this.handleOnChange}
                        disabled={!this.state.password || this.state.password==undefined || this.state.password==''}
                    />
                    {/* (this.state.password!=undefined && this.state.password.length < 1) */}
                    <div className="password_notes" style={{fontSize:10, color:"blue", fontStyle:"italic"}}> Provide same password as above to confirm</div>
                </Form>
                <div className="userNameError" style={{fontSize:12, color:"red", fontStyle:"italic"}}>{this.state.confirmpasswordError}</div>

                {/* <div className="userNameError" style={{fontSize:12, color:"red", fontStyle:"italic"}}></div> */}
                <Button.Group style={{marginTop:20}}>
                    <Button id="register_btn" as={Link} to='/dashboard' style={{marginRight:10}} 
                    disabled={!this.state.userName || !this.state.password || !this.state.confirmpassword 
                        || this.state.disabledRegisterButtonDueToErrors.length>0 || this.state.userName.length<5 
                        || this.state.password.length<5} 
                    onClick={this.handleRegister}>
                        REGISTER
                    </Button>

                    <Button onClick={this.props.handleRegister} href="/">CANCEL</Button>
                    {/* onClick={this.props.history.goBack} */}
                    {/* onClick={props => <Login {...props} />}  */}
                    {/* onClick={this.context.router.goBack()} */}
                    

                </Button.Group>
                </Responsive>
            </div>
        );
    }
}
export default Register;