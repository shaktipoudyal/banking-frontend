import React, { Component } from 'react';
import {Form, Segment, Button as SemanticUIButton} from 'semantic-ui-react'
import {Button} from "react-bootstrap";
import Axios from "axios";
import * as API_CONSTANT from '../ApiConstants';
import CurrencyInput from 'react-currency-input';
import './../landingpage.css';

class LandingPageContent extends Component{
    constructor(props){ //bind methods to component
        super(props);
        this.state={
            bankAccNumber: null,
            renderDeposit: null,
            renderWithdraw: null,
            depositAmount: 0,
            withdrawAmount: 0,
            jwtToken: localStorage.getItem('jwtInLocalStorage') || ' ',
            amountErrorMsg: '',
            successMsg: '',
            transactionAmount: 0,
            status: null,
            existingAmount: null,
           
        };
        this.handleOnChange= this.handleOnChange.bind(this);
        this.handleExistingCustomers= this.handleExistingCustomers.bind(this);
        this.handleRenderingDeposit= this.handleRenderingDeposit.bind(this);
        this.handleRenderingWithdrawal= this.handleRenderingWithdrawal.bind(this);
        this.handleDeposit= this.handleDeposit.bind(this);
        this.handleTransaction= this.handleTransaction.bind(this);
    }

    handleOnChange= (event) =>{
        let obj={};
        obj[event.target.id]=event.target.value;
        console.log(obj)
        console.log(obj.depositAmount)
        console.log(this.state.existingAmount);

        var depositAmount= obj.depositAmount;
        if(depositAmount){
            depositAmount= depositAmount.replace(/,/g, '');
            depositAmount= depositAmount.replace(/[$]/g, '');
            depositAmount= parseFloat(depositAmount);
        }
        if(depositAmount > 100000){
            obj.depositAmount=0;
            this.setState({
                amountErrorMsg: 'Amount must be $100,000 or less'
            })
        }else{
            this.setState({
                amountErrorMsg: '',
                ...this.state.depositAmount
            }) 
            console.log(depositAmount);
        }
        var withdrawalAmount= obj.withdrawAmount;
        if(withdrawalAmount){
            withdrawalAmount= withdrawalAmount.replace(/,/g, '');
            withdrawalAmount= withdrawalAmount.replace(/[$]/g, '');
            withdrawalAmount= parseFloat(withdrawalAmount);
          
            if(withdrawalAmount > this.state.existingAmount){
                obj.withdrawAmount=0;
                this.setState({
                    amountErrorMsg: 'Amount is greater than available balance!',
                }) 
            }else{
                this.setState({
                    amountErrorMsg: '',
                    ...this.state.withdrawAmount
                })  
            }
            console.log(withdrawalAmount);
        }

        this.setState({
            ...this.state.bankAccNumber,
            ...obj
        })
    }

    handleExistingCustomers(event){
        Axios({
            url: API_CONSTANT.BASE_URL + '/api/getByAccountNumber/' + this.state.bankAccNumber,
            method:"get",
            headers:{
                "Content-Type":"application/json",
                "Authorization": "Bearer " + this.state.jwtToken
            }
        }).then(res=>{
            this.setState({
                data: res.data,
                existingAmount: res.data.amount
            })
            console.log(this.state.data)
        }).catch(err=>{
            console.log(err);
        });
    }

    handleTransaction(event){
        console.log(this.state.depositAmount)
        console.log(this.state.withdrawAmount)
        console.log(this.state.bankAccNumber)
        
        this.handleDeposit();
    }

    handleDeposit(event){
        console.log(this.state.depositAmount!=0);
        let amount;
        let endpoint;

        if(this.state.depositAmount!=0){
            amount= this.state.depositAmount;
            amount= amount.replace(/,/g, '');
            amount= amount.replace(/[$]/g, '');
            amount= parseFloat(amount);
            endpoint= '/api/deposit/';

        }else{
            amount= this.state.withdrawAmount;
            amount= amount.replace(/,/g, '');
            amount= amount.replace(/[$]/g, '');
            amount= parseFloat(amount);
            // amount= amount- (amount*2); // making amount negative
            endpoint= '/api/withdraw/';
        }
        console.log(amount);

        Axios({
            url: API_CONSTANT.BASE_URL + endpoint + this.state.bankAccNumber + '/' + amount,
            method:"post",
            headers:{
                "Content-Type":"application/json",
                "Authorization": "Bearer " + this.state.jwtToken
            }
        }).then(res=>{
            console.log(res)
            if(res.status==200){
                this.handleExistingCustomers();
            }
            this.setState({
                status: res.status,
                depositAmount: 0,
                withdrawAmount: 0,
                renderDeposit: null,
                renderWithdraw:  null,
                successMsg: 'Transaction successful! Avaiable balance has been updated.',
            })
        }).catch(err=>{
            console.log(err);
        });
    }

    handleWithdrawal(event){
        Axios({
            url: API_CONSTANT.BASE_URL + '/api/withdraw/' + this.state.bankAccNumber + '/' + this.state.withdrawAmount,
            method:"post",
            headers:{
                "Content-Type":"application/json",
                "Authorization": "Bearer " + this.state.jwtToken
            }
        }).then(res=>{
            this.setState({
                
            })
        }).catch(err=>{
            console.log(err);
        });
    }

    handleRenderingDeposit(event){
       this.setState({
           renderDeposit: true,
           renderWithdraw: false,
           withdrawAmount: null,
           successMsg: ''
       })
    }

    handleRenderingWithdrawal(event){
        this.setState({
            renderWithdraw: true,
            renderDeposit: false,
            depositAmount: 0,
            successMsg: ''
        })
     }

    render(){
        let customerAccountsData= this.state.data;
        const renderCustomerAccounts=()=>{
            if(customerAccountsData){
                return(
                    <div>
                        <div className="customer_Accounts_Info">
                            <span>
                                Account Number: <strong>{customerAccountsData.bankAccNumber}</strong>
                            </span>
                            <br/>
                            <span>
                                Account Type: <strong>{customerAccountsData.accountType}</strong>
                            </span>
                            <br/>
                            <div>
                                Available Balance: <strong>${customerAccountsData.amount}</strong>
                            </div>
                        </div>
                        <div className="deposit_withdraw_btn">
                            <SemanticUIButton.Group>
                                <Button variant="dark" style={{marginRight:10, width:150}} 
                                onClick={this.handleRenderingDeposit}>DEPOSIT</Button>
                            </SemanticUIButton.Group>
                            <SemanticUIButton.Group>
                                <Button variant="dark" style={{width:150}} 
                                onClick={this.handleRenderingWithdrawal}>WITHDRAW</Button>
                            </SemanticUIButton.Group>
                        </div>
                        <div style={{fontSize:16, color:"green", backgroundColor:"white", fontStyle:"italic"}}>{this.state.successMsg}</div>
                        
                    </div>
                );
            }
        }
        const renderDepositScreen=()=>{
          if(this.state.renderDeposit){
              return(
                <div>
                     <label required><strong>Enter Deposit Amount: *</strong></label>
                     <div>
                        <CurrencyInput 
                        id="depositAmount"
                        name="depositAmount"
                        label="Deposit Amount (USD):"
                        prefix="$" 
                        style={{width:250, height:40}} 
                        value={this.state.depositAmount} 
                        onChangeEvent={this.handleOnChange} 
                        // placeholder="Enter deposit amount here"
                        />
                    </div>
                    <div style={{fontSize:12, color:"red", fontStyle:"italic"}}>{this.state.amountErrorMsg}</div>
                </div>
              );
          }
        }

        const renderWithdrawScreen=()=>{
            if(this.state.renderWithdraw){
                return(
                <div>
                        <label required><strong>Enter Withdraw Amount: *</strong></label>
                       <div>
                        <CurrencyInput 
                        id="withdrawAmount"
                        name="withdrawAmount"
                        prefix="$" 
                        style={{width:250, height:40}} 
                        value={this.state.withdrawAmount} 
                        onChangeEvent={this.handleOnChange} 
                        // placeholder="Enter withdraw amount here"
                        />
                    </div>
                    <div style={{fontSize:12, color:"red", fontStyle:"italic"}}>{this.state.amountErrorMsg}</div>

                  </div>
                );
            }
          }

          const renderAmountConfirmationButton=()=>{
              if(this.state.renderDeposit || this.state.renderWithdraw){
                  return(
                      <div>
                        <SemanticUIButton.Group>
                            <Button style={{width:150}} variant="success" 
                            className="landingPage_submit_btn" 
                            disabled={!this.state.bankAccNumber} 
                            onClick={this.handleTransaction}
                            disabled={!this.state.depositAmount && !this.state.withdrawAmount}
                            >CONFIRM</Button>
                        </SemanticUIButton.Group>
                      </div>
                  );
              }
          }
        return(
            <div className="landing_page_content">
                <h5 style={{marginBottom:15}}>Provide customer info below to deposit / withdraw: </h5>
                 <Form className="loginform">
                    <Form.Input className="loginform_username"
                        width={4}
                        fluid
                        required
                        type="number"
                        label="Account Number:"
                        id="bankAccNumber"
                        name="bankAccNumber"
                        placeholder="Enter account number"
                        onChange={this.handleOnChange}
                    />
                </Form>
                <SemanticUIButton.Group>
                    <Button variant="success" style={{width:150}} 
                    className="landingPage_submit_btn" 
                    disabled={!this.state.bankAccNumber || this.state.bankAccNumber.length!=10} 
                    onClick={this.handleExistingCustomers}>SUBMIT</Button>
                </SemanticUIButton.Group>

                <Segment style={{ minHeight: 350, padding: 5}} className="segment1" placeholder>
                {
                    renderCustomerAccounts()
                }
                {
                    renderDepositScreen()
                }
                {
                    renderWithdrawScreen()
                }
                {
                    renderAmountConfirmationButton()
                }
                </Segment>
                
                <div className="footer">
                    <i class="facebook icon big"></i>
                    <i class="twitter icon big"></i>
                    <i class="instagram icon big"></i>
                </div>
                
               
            </div>
        );
    }
}
export default LandingPageContent;