import React, { Component } from 'react';
import {Table} from 'semantic-ui-react';
import {Icon} from 'semantic-ui-react/dist/commonjs';
import {Button} from "react-bootstrap";
import {Modal} from 'semantic-ui-react';
import { rosybrown } from 'color-name';
import UpdateCustomer from './UpdateCustomer';

class ExistingCustomers extends Component{
    constructor(props){
        super(props);
        this.state={
            columns:[
                {"header":"First Name", "name": "firstName"},
                {"header":"Last Name", "name": "lastName"},
                {"header":"SSN", "name": "ssn"},
            ],
            toggleDeleteModal: false,
            data: this.props.rows,
            selectedRow: null
        };
        this.handleDeleteCustomer= this.handleDeleteCustomer.bind(this);
    }

        handleDeleteCustomer = param => e => {
        console.log("handleDeleteCustomer called")
        console.log(e)
        console.log(param)

        this.setState({
            selectedRow: param
        })

        if(this.state.toggleDeleteModal==true){
            this.setState({
                toggleDeleteModal:false
            })
        }else{
            this.setState({
                toggleDeleteModal:true
            })
        }
    }

    render(){
        console.log(this.state.data[0]);
        return(
            <div>
                <Table basic>
                    <Table.Header>
                        <Table.Row>
                            {this.state.columns.map(col=>
                                <Table.HeaderCell key={col.name}>{col.header}</Table.HeaderCell>
                            )}
                            <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                            <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.state.data.map((row, index)=>
                        <Table.Row key={row.id}>
                            <Table.Cell>{row.firstName}</Table.Cell>
                            <Table.Cell>{row.lastName}</Table.Cell>
                            <Table.Cell>{row.ssn}</Table.Cell>
                            <Table.Cell>
                                <Icon name= 'edit' style={{cursor:'pointer'}} color='blue' size='med'></Icon>
                            </Table.Cell>
                            <Table.Cell>
                                <Icon name='delete' onClick={this.handleDeleteCustomer(index)} style={{cursor:'pointer'}} color='red' size='small'></Icon>
                            </Table.Cell>
                        </Table.Row>
                        )}
                    </Table.Body>
                </Table>
                <Modal.Actions>
                        <Button variant="danger" className="btn-2" onClick={this.props.closeForm} href="/dashboard">
                            Close
                        </Button>
                </Modal.Actions>

                <Modal open={this.state.toggleDeleteModal} close={!this.state.toggleDeleteModal}>
                        <Modal.Header content= {"Are you sure you want to permanently delete customer?"}/>
                        <Modal.Content>
                           <UpdateCustomer handleDeleteCustomer={this.handleDeleteCustomer} rows={this.state.data[this.state.selectedRow]}/>
                        </Modal.Content>
                    </Modal>
            </div>
        );
    }
}

export default ExistingCustomers;